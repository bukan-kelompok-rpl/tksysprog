import os


def attention_message():
    msg = "{}\n{}\n{}\n{}\n{}\n{}\n".format(
            "--------------------------------Attention!--------------------------------",
            "Please make sure that the 'alsa-utils' package is installed in your system.",
      		"  Also, this script needs superuser permission to configure the volume.   ",
      		"     So, consider adding `sudo` as a prefix of running this script.       ",
      		"               Example: `sudo python <name-of-file>.py`                   ",
            "--------------------------------------------------------------------------"
    )
    
    return msg

  
def get_commands():
    cmd = "{}\n{}\n{}\n{}\n{}\n{}\n{}\n{}".format(
              "Choose a number (1-7) according to the action you wish to do:",
              "1. Change volume to x%.",
              "2. Increase volume by x%.",
              "3. Decrease volume by x%.",
              "4. Mute audio.",
              "5. Unmute audio.",
              "6. Test sound.",
              "7. exit"
    )

    return cmd


def main():
    print(attention_message())

    while True:
        print(get_commands())

        user_input = eval(input("input>> "))

        if user_input == 7:
            print("Exiting interface...")
            break
        elif user_input == 1:
            value_percentage = str(input("What volume do you want to set it to?: "))
            os.system("amixer -q sset Master " + value_percentage + "%")
            print("Volume set to {}%.".format(value_percentage))
        elif user_input == 2:
            value_percentage = str(input("How much volume do you want to increase(%): "))
            os.system("amixer -q sset Master " + value_percentage + "%+")
            print("Volume increased by {}%.".format(value_percentage))
        elif user_input == 3:
            value_percentage = str(input("How much volume do you want to decrease(%): "))
            os.system("amixer -q sset Master " + value_percentage + "%-")
            print("Volume decreased by {}%.".format(value_percentage))
        elif user_input == 4:
            os.system("amixer -q set Master mute")
            print("Mute success.")
        elif user_input == 5:
            os.system("amixer -q set Master unmute")
            print("Unmute success.")
        elif user_input == 6:
            os.system("speaker-test -Ddefault:Intel -c 2 -l 1")
        else:
            print("Please input a number one of 1-7.")

        print()

    exit()

          
if __name__ == '__main__':
    main()
